use serde_derive::{Serialize, Deserialize};
use fluffy::{model::Model};

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct UserCodes {
    pub id: usize,
    pub user_id: usize,
    pub user_name: String,
    pub code: String,
    pub created: String,
}

impl Model for UserCodes {
    fn get_table_name() -> &'static str {
        &"user_codes"
    }
}
