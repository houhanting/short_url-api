use serde_derive::{Deserialize, Serialize};
use fluffy::{model::Model};

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct UserLogin {
    pub name: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct UserRegister {
    pub name: String, //用户名称
    //pub mail: String, //电子邮件
    pub password: String, //密码
    pub re_password: String, //重复密码
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Users {
    pub id: usize, //用户编号
    pub name: String, //用户名称
    pub last_login: String, //最后登录时间
    pub last_ip: String, //最后登录ip
    pub login_count: usize, //登录次数
    pub created: String, //主册时间
}

impl Model for Users { 
    fn get_table_name() -> &'static str { 
        &"users"
    }
}
