use serde_derive::{Serialize};
use fluffy::{model::{Model}, Pager};

#[derive(Debug, Default, Serialize)]
pub struct Links {
    pub id: usize,
    //pub ymd: usize,
    //pub user_id: usize,
    //pub user_name: String,
    pub origin_url: String,
    pub code: String,
    pub image: String,
    pub created: String,
}

#[derive(Debug, Default, Serialize)]
pub struct LinksPager { 
    pub rows: Vec<Links>,
    pub pager: Pager,
}

impl Model for Links {
    fn get_table_name() -> &'static str {
        &"links"
    }
}
