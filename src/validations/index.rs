use actix_web::{web::Json};
use fluffy::{validation::Validator};
use crate::models::{UserRegister, UserLogin};

/// 注册新用户的校验
pub fn register(data: &Json<UserRegister>) -> Result<(), String> {
    Validator::new()
        .is_username(&data.name, "请输入合法的用户名称")
        .is_password(&data.password, "请输入正确格式的密码")
        .is_password(&data.re_password, "请输入正确格式的密码")
        .is_re_password(&data.password, &data.re_password, "两次输入的密码必须一致")
        .validate()
}

/// 登录的数据校验
pub fn login(data: &Json<UserLogin>) -> Result<(), String> {
    Validator::new()
        .is_username(&data.name, "请输入合法的用户名称")
        .is_password(&data.password, "请输入正确格式的密码")
        .validate()
}
