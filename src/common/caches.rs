use redis::Commands;
use crate::models::UserCodes;
use fluffy::{model::Model, db};

/// 初始化用户keys
pub fn init_user_keys() { 
    let query = query![
        fields => "user_id, user_name, code",
    ];
    let mut conn = db::get_conn();
    let mut cache = fluffy::cache::get_conn();
    let rows = UserCodes::fetch_rows(&mut conn, &query, None);
    let hash_key = "user-keys";
    for r in rows { 
        let (id, name, code): (usize, String, String) = from_row!(r);
        let val = format!("{}|{}", id, name);
        println!("加载缓存数据: {}[] => {} => {}", hash_key, code, val);
        cache.hset::<&str, &str, &str, bool>(hash_key, &code, &val).unwrap();
    }
}
