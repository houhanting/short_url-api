pub mod caches;

use serde_derive::{Deserialize, Serialize};

/// 网站域名
pub const HOST_NAME: &str = "http://urls.ph";

/// redis连接
pub const REDIS_CONN_STR: &str = "redis://127.0.0.1";

/// 数据库连接
pub const MYSQL_CONN_STR: &str = "mysql://short_link:short-x-lsl@localhost:3306/short_links";

/// 最多登录出错次数
pub const LOGIN_ERROR_MAX: usize = 5;

/// 登第第一次错误
pub const LOGIN_ERROR_FIRST: &str = "error-first";

/// 登录出错误KEY
pub const LOGIN_ERROR_KEY: &str = "login-error";

/// 未登录用户每ip第天调用次数
pub const NOT_LOGIN_CALL_COUNT: usize = 100; 

/// 登录用户每天调用次数
pub const LOGIN_CALL_COUNT: usize = 1000;

/// 短网址保存的过期时间
pub const URL_EXPIRED: usize = 86400 * 10;

/// 短链接结果
#[derive(Debug, Serialize, Deserialize, Default)]
pub struct ShortLink {
    pub url: String, //生成的短链接地址
    pub code: String, //生成的二维码base64字符串
}

/// 用户登录的token
#[derive(Debug, Serialize, Deserialize, Default)]
pub struct LoginToken {
    pub token: String,
}
